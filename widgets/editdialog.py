from PyQt5.QtCore import pyqtSlot
from PyQt5.QtSql import QSqlQuery
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from widgets.database import MyConnection
from widgets.functions import clean_input
from widgets.regex import MyRegExValid


class EditDialog(QDialog):
    def __init__(self, model, table_name, real_index, parent=None):
        super(EditDialog, self).__init__(parent)
        self.db = MyConnection.db
        self.model = model
        self.table_name = table_name.lower()
        self.real_index = real_index
        self.real_row = real_index.row()
        self.ui = loadUi('uis/' + self.table_name + '.ui', self)
        self.setWindowTitle('Edytuj')
        self.accepted.connect(self.on_accept)

        if self.table_name in ['czesc', 'specjalizacja', 'status', 'zajezdnia']:
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)

            # Set up data
            if self.table_name == 'zajezdnia':
                self.lineEdit.setText(self.model.index(self.real_row, 1).data())
            else:
                self.lineEdit.setText(self.model.index(self.real_row, 0).data())

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)

        elif self.table_name == 'etat':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)
            self.lineEdit_2.setValidator(MyRegExValid.decimal72)

            # Set up data
            self.lineEdit.setText(self.model.index(self.real_row, 0).data())
            self.lineEdit_2.setText(str(self.model.index(self.real_row, 1).data()))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)

        elif self.table_name in ['model', 'typ_usterki']:
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)
            self.lineEdit_2.setValidator(MyRegExValid.int10unsigned)

            # Set up data
            self.lineEdit.setText(self.model.index(self.real_row, 0).data())
            self.lineEdit_2.setText(str(self.model.index(self.real_row, 1).data()))
            if table_name == 'model':
                self.checkBox.setChecked(bool(self.model.index(self.real_row, 2).data()))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)

        elif self.table_name == 'pracownik':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)
            self.lineEdit_2.setValidator(MyRegExValid.varchar64)

            # Set up data
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT ETAT FROM ETAT')
            while combo_query.next():
                self.comboBox.addItem(combo_query.value(0))
            combo_query.exec_('SELECT SPEC FROM SPECJALIZACJA')
            self.comboBox_2.addItem("")
            while combo_query.next():
                self.comboBox_2.addItem(combo_query.value(0))

            self.lineEdit.setText(self.model.index(self.real_row, 3).data())
            self.lineEdit_2.setText(self.model.index(self.real_row, 4).data())
            self.comboBox_2.setCurrentText(self.model.index(self.real_row, 1).data())
            self.comboBox.setCurrentText(self.model.index(self.real_row, 2).data())
            self.calendarWidget.setSelectedDate(self.model.index(self.real_row, 5).data())

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)
            self.comboBox.currentTextChanged.connect(self.update_button_status)

        elif self.table_name == 'stan_czesci':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.int10unsigned)
            self.lineEdit_2.setValidator(MyRegExValid.decimal82)
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT NAZWA FROM MODEL')
            while combo_query.next():
                self.comboBox.addItem(combo_query.value(0))
            combo_query.exec_('SELECT CZESC FROM CZESC')
            while combo_query.next():
                self.comboBox_2.addItem(combo_query.value(0))

            # Set up data
            self.comboBox.setCurrentText(self.model.index(self.real_row, 0).data())
            self.comboBox_2.setCurrentText(self.model.index(self.real_row, 1).data())
            self.lineEdit.setText(str(self.model.index(self.real_row, 2).data()))
            self.lineEdit_2.setText(str(self.model.index(self.real_row, 3).data()))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)

        elif self.table_name == 'tramwaj':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.year4)
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT NAZWA FROM MODEL')
            while combo_query.next():
                self.comboBox.addItem(combo_query.value(0))

            # Set up data
            self.comboBox.setCurrentText(self.model.index(self.real_row, 1).data())
            self.lineEdit.setText(str(self.model.index(self.real_row, 2).data()))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)

        elif self.table_name == 'przydzial':
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT ID_PRAC FROM PRACOWNIK ORDER BY ID_PRAC')
            while combo_query.next():
                self.comboBox.addItem(str(combo_query.value(0)))

            combo_query.exec_('SELECT ID_USTER FROM USTERKA ORDER BY ID_USTER')
            while combo_query.next():
                self.comboBox_2.addItem(str(combo_query.value(0)))

            # Set up data
            self.comboBox.setCurrentText(str(self.model.index(self.real_row, 0).data()))
            self.comboBox_2.setCurrentText(str(self.model.index(self.real_row, 1).data()))

        elif self.table_name == 'usterka':
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT ID_TRAM FROM TRAMWAJ')
            while combo_query.next():
                self.comboBox.addItem(str(combo_query.value(0)))

            combo_query.exec_('SELECT TYP_USTERKI FROM TYP_USTERKI')
            while combo_query.next():
                self.comboBox_2.addItem(str(combo_query.value(0)))

            combo_query.exec_('SELECT ID_ZAJEZD FROM ZAJEZDNIA')
            while combo_query.next():
                self.comboBox_3.addItem(str(combo_query.value(0)))

            combo_query.exec_('SELECT STATUS FROM STATUS')
            while combo_query.next():
                self.comboBox_4.addItem(str(combo_query.value(0)))

            # Set up data
            self.comboBox.setCurrentText(str(self.model.index(self.real_row, 1).data()))
            self.comboBox_2.setCurrentText(str(self.model.index(self.real_row, 2).data()))
            self.comboBox_3.setCurrentText(str(self.model.index(self.real_row, 3).data()))
            self.comboBox_4.setCurrentText(str(self.model.index(self.real_row, 4).data()))
            self.calendarWidget.setSelectedDate(self.model.index(self.real_row, 5).data())

    @pyqtSlot()
    def on_accept(self):
        if self.table_name in ['czesc', 'specjalizacja', 'status', 'zajezdnia']:
            if self.table_name == 'zajezdnia':
                self.model.setData(self.model.index(self.real_row, 1), clean_input(self.lineEdit.text()).upper())
            else:
                self.model.setData(self.model.index(self.real_row, 0), clean_input(self.lineEdit.text()).upper())

        if self.table_name == 'etat':
            self.model.setData(self.model.index(self.real_row, 0), clean_input(self.lineEdit.text()).upper())
            self.model.setData(self.model.index(self.real_row, 1), float(clean_input(self.lineEdit_2.text())))

        elif self.table_name in ['model', 'typ_usterki']:
            self.model.setData(self.model.index(self.real_row, 0), clean_input(self.lineEdit.text()).upper())
            self.model.setData(self.model.index(self.real_row, 1), int(clean_input(self.lineEdit_2.text())))
            if self.table_name == 'model':
                self.model.setData(self.model.index(self.real_row, 2), int(self.checkBox.isChecked()))

        elif self.table_name == 'pracownik':
            new_spec = self.comboBox_2.currentText()
            if new_spec != "":
                self.model.setData(self.model.index(self.real_row, 1), new_spec)
            else:
                self.model.setData(self.model.index(self.real_row, 1), None)
            self.model.setData(self.model.index(self.real_row, 2), self.comboBox.currentText())
            self.model.setData(self.model.index(self.real_row, 3), clean_input(self.lineEdit.text()).upper())
            self.model.setData(self.model.index(self.real_row, 4), clean_input(self.lineEdit_2.text()).upper())
            self.model.setData(self.model.index(self.real_row, 5), self.calendarWidget.selectedDate())

        elif self.table_name == 'stan_czesci':
            self.model.setData(self.model.index(self.real_row, 0), self.comboBox.currentText())
            self.model.setData(self.model.index(self.real_row, 1), self.comboBox_2.currentText())
            self.model.setData(self.model.index(self.real_row, 2), int(clean_input(self.lineEdit.text())))
            self.model.setData(self.model.index(self.real_row, 3), float(clean_input(self.lineEdit_2.text())))

        elif self.table_name == 'tramwaj':
            self.model.setData(self.model.index(self.real_row, 1), self.comboBox.currentText())
            self.model.setData(self.model.index(self.real_row, 2), int(clean_input(self.lineEdit.text())))

        elif self.table_name == 'przydzial':
            self.model.setData(self.model.index(self.real_row, 0), int(self.comboBox.currentText()))
            self.model.setData(self.model.index(self.real_row, 1), int(self.comboBox_2.currentText()))

        elif self.table_name == 'usterka':
            self.model.setData(self.model.index(self.real_row, 1), int(self.comboBox.currentText()))
            self.model.setData(self.model.index(self.real_row, 2), self.comboBox_2.currentText())
            self.model.setData(self.model.index(self.real_row, 3), int(self.comboBox_3.currentText()))
            self.model.setData(self.model.index(self.real_row, 4), self.comboBox_4.currentText())
            self.model.setData(self.model.index(self.real_row, 5), self.calendarWidget.selectedDate())

    @pyqtSlot(str)
    def update_button_status(self, _):
        conditions = []
        if self.table_name in ['czesc', 'specjalizacja', 'status', 'tramwaj', 'zajezdnia']:
            conditions = [clean_input(self.lineEdit.text()) not in ["", " "]]
        elif self.table_name in ['etat', 'model', 'stan_czesci', 'typ_usterki']:
            conditions = [clean_input(self.lineEdit.text()) not in ["", " "],
                          clean_input(self.lineEdit_2.text()) not in ["", " "]]
        elif self.table_name == 'pracownik':
            conditions = [clean_input(self.lineEdit.text()) not in ["", " "],
                          clean_input(self.lineEdit_2.text()) not in ["", " "],
                          clean_input(self.comboBox.currentText()) not in ["", " "]]

        if all(conditions):
            self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(True)
        else:
            self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(False)
