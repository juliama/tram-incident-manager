from PyQt5.QtSql import QSqlQuery
from PyQt5.QtWidgets import QPushButton, QMessageBox
from PyQt5.QtCore import pyqtSignal

from widgets.database import MyConnection


class AddPushButton(QPushButton):
    new_record = pyqtSignal()

    def __init__(self, parent):
        super(AddPushButton, self).__init__(parent)
        self.clicked.connect(self.on_click)

    def on_click(self):
        self.new_record.emit()


class AssignPushButton(QPushButton):
    new_assignment = pyqtSignal()

    def __init__(self, parent):
        super(AssignPushButton, self).__init__(parent)
        self.clicked.connect(self.on_click)

    def on_click(self):
        self.new_assignment.emit()


class IncidentCountPushButton(QPushButton):
    def __init__(self, parent):
        super(IncidentCountPushButton, self).__init__(parent)
        self.clicked.connect(self.on_click)
        self.db = MyConnection.db

    def on_click(self):
        typ = ""
        for child in self.parent().children():
            if child.objectName() == 'comboBox_10':
                typ = child.currentText()

        query = QSqlQuery(self.db)
        q = f"SELECT ZLICZ_USTERKI('{typ}')"
        query.exec_(q)
        message = QMessageBox()
        if query.lastError().type():
            message.setText("Nie udało się zliczyć usterek.")
        else:
            query.next()
            message.setText("Znaleziono " + str(query.value(0)) + " usterek.")
        message.exec_()


class ChangeStatusPushButton(QPushButton):
    def __init__(self, parent):
        super(ChangeStatusPushButton, self).__init__(parent)
        self.clicked.connect(self.on_click)
        self.db = MyConnection.db

    def on_click(self):
        typ1 = ""
        typ2 = ""
        val = 0
        for child in self.parent().children():
            if child.objectName() == 'comboBox_8':
                typ1 = child.currentText()
            if child.objectName() == 'comboBox_9':
                typ2 = child.currentText()
            if child.objectName() == 'horizontalSlider':
                val = child.value()

        if typ1 == "" or typ2 == "":
            return

        query = QSqlQuery(self.db)
        q = f"CALL PRZENIES_ZALEGAJACE({val}, '{typ2}', '{typ1}')"
        query.exec_(q)

        message = QMessageBox()
        if query.lastError().type():
            message.setText("Nie udało się zaktualizować statusów.")
        else:
            message.setText("Zaktualizowano statusy.")
        message.exec_()


class MinimumWagePushButton(QPushButton):
    def __init__(self, parent):
        super(MinimumWagePushButton, self).__init__(parent)
        self.clicked.connect(self.on_click)
        self.db = MyConnection.db

    def on_click(self):
        min_wage = 0
        for child in self.parent().children():
            if child.objectName() == 'lineEdit_7':
                x = child.text()
                if x == '':
                    return
                else:
                    min_wage = float(x)

        query = QSqlQuery(self.db)
        q = f"CALL PODNIES_PLAC_MIN({min_wage})"
        query.exec_(q)
        message = QMessageBox()
        if query.lastError().type():
            message.setText("Nie udało się zaktualizować płacy minimalnej.")
        else:
            for child in self.parent().children():
                if child.objectName() == 'lineEdit_7':
                    child.setText('')
            message.setText("Zaktualizowano płacę minimlaną.")
        message.exec_()

