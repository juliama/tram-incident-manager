from PyQt5.QtCore import QSize
from PyQt5.QtWidgets import QMainWindow
from PyQt5.uic import loadUi

from widgets.regex import MyRegExValid


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = loadUi('uis/main.ui', self)
        self.setWindowTitle('Tram Incident Manager')
        self.setFixedSize(QSize(1073, 630))

        # "Dane" tab
        # ComboBox signal to slot
        self.comboBox.table_changed.connect(self.tableView.change_model)
        self.comboBox.table_changed.connect(self.comboBox_2.update_data)

        self.comboBox_2.column_changed.connect(self.tableView.filter_changed)

        # PushButton signal to slot
        self.pushButton_2.clicked.connect(self.tableView.delete_record)
        self.pushButton.new_record.connect(self.tableView.add_record)

        # LineEdit signal to slot
        self.lineEdit.setValidator(MyRegExValid.search_bar)
        self.lineEdit.textChanged.connect(self.tableView.filter_changed)

        # TabWidget signal to slot
        self.tabWidget.currentChanged.connect(self.tableView.update_model)

        # "Przypisz pracownika" tab
        # TableView setup
        self.tableView_3.table = 'PRACOWNIK_DO_PRZYDZIALU'
        self.tableView_3.update_model()

        self.tableView_4.table = 'USTERKA_DO_PRZYDZIALU'
        self.tableView_4.update_model()

        self.tableView_2.table = 'PRZYDZIAL_INFO'
        self.tableView_2.update_model()

        self.tableView_2.tableView_prac = self.tableView_3
        self.tableView_2.tableView_uster = self.tableView_4

        # LineEdit signal to slot
        self.lineEdit.setValidator(MyRegExValid.search_bar)
        self.lineEdit_2.setValidator(MyRegExValid.search_bar)
        self.lineEdit_3.setValidator(MyRegExValid.search_bar)
        self.lineEdit_4.setValidator(MyRegExValid.search_bar)
        self.lineEdit_2.textChanged.connect(self.tableView_2.filter_changed)
        self.lineEdit_3.textChanged.connect(self.tableView_3.filter_changed)
        self.lineEdit_4.textChanged.connect(self.tableView_4.filter_changed)

        # ComboBox signal to slot
        self.comboBox_4.assign_column_changed.connect(self.tableView_2.filter_changed)

        # PushButton signal to slot
        self.pushButton_8.clicked.connect(self.tableView_2.delete_assignment)
        self.pushButton_4.clicked.connect(self.tableView_2.add_assignment)

        # TabWidget signal to slot
        self.tabWidget.currentChanged.connect(self.tableView_2.update_model)
        self.tabWidget.currentChanged.connect(self.tableView_3.update_model)
        self.tabWidget.currentChanged.connect(self.tableView_4.update_model)

        # "Dodaj usterkę" tab
        self.tableView_6.table = 'TRAMWAJ'
        self.tableView_6.update_model()

        self.tableView_13.table = 'USTERKA'
        self.tableView_13.update_model()

        # TabWidget signal to slot
        self.tabWidget.currentChanged.connect(self.tableView_6.update_model)
        self.tabWidget.currentChanged.connect(self.tableView_13.update_model)
        self.tabWidget.currentChanged.connect(self.comboBox_5.update_data)
        self.tabWidget.currentChanged.connect(self.comboBox_6.update_data)
        self.tabWidget.currentChanged.connect(self.comboBox_7.update_data)

        # PushButton signal to slot
        self.pushButton_5.clicked.connect(self.tableView_13.add_incident)
        self.pushButton_6.clicked.connect(self.tableView_13.delete_incident)

        # LineEdit signal to slot
        self.lineEdit_5.setValidator(MyRegExValid.search_bar)
        self.lineEdit_6.setValidator(MyRegExValid.search_bar)
        self.lineEdit_5.textChanged.connect(self.tableView_13.filter_changed)
        self.lineEdit_6.textChanged.connect(self.tableView_6.filter_changed)

        # ComboBox signal to slot
        self.comboBox_15.incident_column_changed.connect(self.tableView_13.filter_changed)

        # "Raporty" tab
        # TableView setup
        self.tableView_5.table = 'USTERKI_BEZ_PRAC'
        self.tableView_5.update_model()

        self.comboBox_3.report_changed.connect(self.tableView_5.change_model)

        # TabWidget signal to slot
        self.tabWidget.currentChanged.connect(self.tableView_5.update_model)

        # "Akcje" tab
        self.horizontalSlider.valueChanged.connect(self.label_17.update_value)
        self.lineEdit_7.setValidator(MyRegExValid.decimal72)

        # TabWidget signal to slot
        self.tabWidget.currentChanged.connect(self.comboBox_8.update_data)
        self.tabWidget.currentChanged.connect(self.comboBox_9.update_data)
        self.tabWidget.currentChanged.connect(self.comboBox_10.update_data)
