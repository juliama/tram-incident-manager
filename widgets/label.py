from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QLabel


class SliderLabel(QLabel):
    def __init__(self, parent):
        super(SliderLabel, self).__init__(parent)
        self.setText('0%')

    @pyqtSlot(int)
    def update_value(self, val):
        self.setText(str(val) + '%')
