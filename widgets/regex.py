from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator


class MyRegExValid:
    varchar64 = QRegExpValidator(QRegExp(r"^[a-zA-Z0-9_ ]*$"))
    decimal72 = QRegExpValidator(QRegExp(r"^[1-9][0-9]{1,4}\.[0-9]{2}$"))
    int10unsigned = QRegExpValidator(QRegExp(r"[0-9]{10}"))
    decimal82 = QRegExpValidator(QRegExp(r"^[1-9][0-9]{1,5}\.[0-9]{2}$"))
    year4 = QRegExpValidator(QRegExp(r"^[1-9][0-9]{3}"))
    search_bar = QRegExpValidator(QRegExp(r"^[a-zA-Z0-9_\- ]*$"))
