from PyQt5.QtCore import pyqtSlot
from PyQt5.QtSql import QSqlQuery
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog
from PyQt5.uic import loadUi

from widgets.database import MyConnection
from widgets.functions import clean_input
from widgets.regex import MyRegExValid


class AddDialog(QDialog):
    error_type = 0

    def __init__(self, model, table_name, parent=None):
        super(AddDialog, self).__init__(parent)
        self.db = MyConnection.db
        self.model = model
        self.table_name = table_name.lower()
        self.ui = loadUi('uis/' + self.table_name + '.ui', self)
        self.setWindowTitle('Dodaj')
        self.record = self.model.record()
        self.accepted.connect(self.on_accept)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(False)

        if self.table_name in ['czesc', 'specjalizacja', 'status', 'zajezdnia']:
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)

        elif self.table_name == 'etat':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)
            self.lineEdit_2.setValidator(MyRegExValid.decimal72)

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)

        elif self.table_name in ['model', 'typ_usterki']:
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)
            self.lineEdit_2.setValidator(MyRegExValid.int10unsigned)

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)

        elif self.table_name == 'pracownik':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.varchar64)
            self.lineEdit_2.setValidator(MyRegExValid.varchar64)
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT ETAT FROM ETAT')
            while combo_query.next():
                self.comboBox.addItem(combo_query.value(0))
            combo_query.exec_('SELECT SPEC FROM SPECJALIZACJA')
            self.comboBox_2.addItem("")
            while combo_query.next():
                self.comboBox_2.addItem(combo_query.value(0))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)
            self.comboBox.currentTextChanged.connect(self.update_button_status)

        elif self.table_name == 'stan_czesci':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.int10unsigned)
            self.lineEdit_2.setValidator(MyRegExValid.decimal82)
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT NAZWA FROM MODEL')
            while combo_query.next():
                self.comboBox.addItem(combo_query.value(0))
            combo_query.exec_('SELECT CZESC FROM CZESC')
            while combo_query.next():
                self.comboBox_2.addItem(combo_query.value(0))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)
            self.lineEdit_2.textChanged.connect(self.update_button_status)

        elif self.table_name == 'tramwaj':
            # Validators
            self.lineEdit.setValidator(MyRegExValid.year4)
            combo_query = QSqlQuery(self.db)
            combo_query.exec_('SELECT NAZWA FROM MODEL')
            while combo_query.next():
                self.comboBox.addItem(combo_query.value(0))

            # Signals
            self.lineEdit.textChanged.connect(self.update_button_status)

    @pyqtSlot()
    def on_accept(self):
        if self.table_name in ['czesc', 'specjalizacja', 'status', 'zajezdnia']:
            if self.table_name == 'zajezdnia':
                self.record.setValue(1, clean_input(self.lineEdit.text()).upper())
            else:
                self.record.setValue(0, clean_input(self.lineEdit.text()).upper())
            self.model.insertRecord(-1, self.record)

        elif self.table_name == 'etat':
            self.record.setValue('etat', clean_input(self.lineEdit.text()).upper())
            self.record.setValue('placa', float(clean_input(self.lineEdit_2.text())))
            self.model.insertRecord(-1, self.record)

        elif self.table_name in ['model', 'typ_usterki']:
            self.record.setValue(0, clean_input(self.lineEdit.text()).upper())
            self.record.setValue(1, int(clean_input(self.lineEdit_2.text())))
            if self.table_name == 'model':
                self.record.setValue('niskopodlogowy', int(self.checkBox.isChecked()))
            self.model.insertRecord(-1, self.record)

        elif self.table_name == 'pracownik':
            if self.comboBox_2.currentText() != "":
                self.record.setValue('spec', self.comboBox_2.currentText())
            else:
                self.record.setValue('spec', None)
            self.record.setValue('etat', self.comboBox.currentText())
            self.record.setValue('imie', clean_input(self.lineEdit.text()).upper())
            self.record.setValue('nazwisko', clean_input(self.lineEdit_2.text()).upper())
            sd = self.calendarWidget.selectedDate()
            self.record.setValue('data_zatr', sd)
            self.model.insertRecord(-1, self.record)

        elif self.table_name == 'stan_czesci':
            self.record.setValue('model', self.comboBox.currentText())
            self.record.setValue('czesc', self.comboBox_2.currentText())
            self.record.setValue('liczba', int(clean_input(self.lineEdit.text()).upper()))
            self.record.setValue('cena_jedn', float(clean_input(self.lineEdit_2.text()).upper()))
            self.model.insertRecord(-1, self.record)

        elif self.table_name == 'tramwaj':
            self.record.setValue('model', self.comboBox.currentText())
            self.record.setValue('rok_produkcji', int(clean_input(self.lineEdit.text())))
            self.model.insertRecord(-1, self.record)

    @pyqtSlot(str)
    def update_button_status(self, _):
        conditions = []
        if self.table_name in ['czesc', 'specjalizacja', 'status', 'tramwaj', 'zajezdnia']:
            conditions = [clean_input(self.lineEdit.text()) not in ["", " "]]
        elif self.table_name in ['etat', 'model', 'stan_czesci', 'typ_usterki']:
            conditions = [clean_input(self.lineEdit.text()) not in ["", " "],
                          clean_input(self.lineEdit_2.text()) not in ["", " "]]
        elif self.table_name == 'pracownik':
            conditions = [clean_input(self.lineEdit.text()) not in ["", " "],
                          clean_input(self.lineEdit_2.text()) not in ["", " "],
                          clean_input(self.comboBox.currentText()) not in ["", " "]]

        if all(conditions):
            self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(True)
        else:
            self.buttonBox.button(QtWidgets.QDialogButtonBox.Ok).setEnabled(False)


