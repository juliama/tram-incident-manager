from PyQt5.QtCore import QSortFilterProxyModel, pyqtSlot, QRegExp, Qt
from PyQt5.QtSql import QSqlTableModel, QSqlQuery
from PyQt5.QtWidgets import QTableView, QAbstractItemView, QHeaderView, QMessageBox

from widgets.database import MyConnection


class AssignWorkerTableView(QTableView):
    table = None

    def __init__(self, parent):
        super(AssignWorkerTableView, self).__init__(parent)
        # Database and model
        self.db = MyConnection.db
        self.model = QSqlTableModel(db=self.db)
        self.proxy = QSortFilterProxyModel(self.model)

        # TableView settings
        self.setSortingEnabled(True)
        self.verticalHeader().setVisible(False)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        # TableModel settings
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)

        # ProxyModel settings
        self.proxy.setFilterKeyColumn(-1)

    @pyqtSlot(int)
    def update_model(self, i=1000):
        if i != 1000:
            if self.table == 'TRAMWAJ' and i == 2:
                pass
            elif self.table != 'TRAMWAJ' and i == 1:
                pass
            else:
                return
        self.model.setTable(self.table)
        self.model.select()
        self.proxy.setSourceModel(self.model)
        self.setModel(self.proxy)

    @pyqtSlot(str)
    def filter_changed(self, text):
        search = QRegExp(text, cs=Qt.CaseInsensitive)
        self.proxy.setFilterRegExp(search)


class AssignmentsTableView(QTableView):
    tableView_prac = None
    tableView_uster = None
    table = None

    def __init__(self, parent):
        super(AssignmentsTableView, self).__init__(parent)
        # Database and model
        self.db = MyConnection.db
        self.model = QSqlTableModel(db=self.db)
        self.proxy = QSortFilterProxyModel(self.model)

        # TableView settings
        self.setSortingEnabled(True)
        self.verticalHeader().setVisible(False)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        # TableModel settings
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)

        # ProxyModel settings
        self.proxy.setFilterKeyColumn(-1)

    @pyqtSlot(int)
    def update_model(self, i=1000):
        if i not in [1, 1000]:
            return
        self.model.setTable(self.table)
        self.model.select()
        self.proxy.setSourceModel(self.model)
        self.setModel(self.proxy)

    @pyqtSlot(str)
    def filter_changed(self, text):
        sender = self.sender().objectName()
        if sender == 'lineEdit_2':
            search = QRegExp(text, cs=Qt.CaseInsensitive)
            self.proxy.setFilterRegExp(search)
        elif sender == 'comboBox_4':
            self.proxy.setFilterKeyColumn(int(text))

    @pyqtSlot()
    def add_assignment(self):
        real_index_3 = self.tableView_prac.proxy.mapToSource(self.tableView_prac.currentIndex())
        real_row_3 = real_index_3.row()
        real_record_3 = self.tableView_prac.model.record(real_row_3)
        val_3 = real_record_3.value('ID_PRAC')

        real_index_4 = self.tableView_uster.proxy.mapToSource(self.tableView_uster.currentIndex())
        real_row_4 = real_index_4.row()
        real_record_4 = self.tableView_uster.model.record(real_row_4)
        val_4 = real_record_4.value('ID_USTER')

        if not val_3 or not val_4:
            return

        query = QSqlQuery(self.db)
        query.exec_(f"INSERT INTO PRZYDZIAL VALUES({val_3}, {val_4})")

        error_type = query.lastError().type()
        error_msg = query.lastError().databaseText()

        # Update view
        self.update_model()

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można dodać przydziału.")
            message.exec_()

    @pyqtSlot(bool)
    def delete_assignment(self):
        real_index = self.proxy.mapToSource(self.currentIndex())
        real_row = real_index.row()
        real_record = self.model.record(real_row)
        val_prac = real_record.value('PRAC_ID')
        val_uster = real_record.value('USTERKA_ID')

        if not val_prac or not val_uster:
            return

        query = QSqlQuery(self.db)
        query.exec_(f"DELETE FROM PRZYDZIAL "
                    f"WHERE PRAC_ID = {val_prac} "
                    f"AND USTERKA_ID = {val_uster}")

        error_type = query.lastError().type()
        error_msg = query.lastError().databaseText()

        # Update view
        self.update_model()

        # Display error message
        if error_type != 0:
            print(error_msg)
            message = QMessageBox()
            message.setText("Nie można usunąć przydziału.")
            message.exec_()
