DROP PROCEDURE IF EXISTS PODNIES_PLAC_MIN;
DROP PROCEDURE IF EXISTS PRZENIES_ZALEGAJACE;

CREATE PROCEDURE PODNIES_PLAC_MIN(IN nowa_placa DECIMAL(7,2))
  BEGIN
    UPDATE ETAT
      SET PLACA = nowa_placa
    WHERE PLACA < nowa_placa;
  END;

CREATE PROCEDURE PRZENIES_ZALEGAJACE(IN max_opoznienie DECIMAL(5,2), IN new_status VARCHAR(64), IN move_from VARCHAR(64))
  BEGIN
    DECLARE done BOOLEAN DEFAULT FALSE;
    DECLARE czas_oczek INTEGER DEFAULT 0;
    DECLARE czas_obecny INTEGER DEFAULT 0;
    DECLARE usterka ROW (ID_USTER INTEGER UNSIGNED, TYP VARCHAR (64), STATUS VARCHAR (64), DATA DATE );
    DECLARE uster_cur CURSOR FOR SELECT
                                   ID_USTER,
                                   TYP,
                                   STATUS,
                                   DATA
                                 FROM USTERKA
                                 WHERE STATUS = move_from;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    OPEN uster_cur;

    WHILE NOT done DO
      FETCH uster_cur
      INTO usterka;

      SELECT OCZEK_CZAS_NAPRAWY
      FROM TYP_USTERKI
      WHERE TYP_USTERKI = usterka.TYP
      INTO czas_oczek;

      SET czas_obecny = DAYS_FROM_DATE(usterka.DATA);

      IF czas_obecny > czas_oczek * max_opoznienie / 100
      THEN
        UPDATE USTERKA
        SET STATUS = new_status
        WHERE ID_USTER = usterka.ID_USTER;
      END IF;
    END WHILE;

    CLOSE uster_cur;
  END;
